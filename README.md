<!--
README.md
Copyright 2023 Keith Maxwell
SPDX-License-Identifier: CC-BY-SA-4.0
-->

# README

This tool is a workaround for a feature of `ssh-add`:

> Identity files should not be readable by anyone but the user. Note that
> ssh-add ignores identity files if they are accessible by others.

-- `man ssh-add`

This is normally not a good idea.

## Installation

Command to install with [uv]:

    uv tool install --index-url https://gitlab.com/api/v4/projects/47411046/packages/pypi/simple ssh-add-insecure

Command to install with [pipx]:

    pipx install --index-url https://gitlab.com/api/v4/projects/47411046/packages/pypi/simple ssh-add-insecure

[uv]: https://github.com/astral-sh/uv
[pipx]: https://github.com/pypa/pipx

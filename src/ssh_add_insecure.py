# ssh_add_insecure.py
# Copyright 2023 Keith Maxwell
# SPDX-License-Identifier: MPL-2.0
"""Add an SSH key to the agent from a temporary directory"""
from argparse import ArgumentParser
from argparse import RawDescriptionHelpFormatter
from pathlib import Path
from shutil import copy
from subprocess import run
from tempfile import TemporaryDirectory


__version__ = "2023.12.04"

NAMES = ("id_ed25519", "id_rsa")


WARNING = """================================================================

WARNING:

> Identity files should not be readable by anyone but the user.

-- `man ssh-add`

================================================================
"""


def main(arguments: list[str] | None = None) -> int:
    parser = ArgumentParser(
        description=__doc__,
        epilog=WARNING,
        formatter_class=RawDescriptionHelpFormatter,
    )
    parser.add_argument("--version", action="version", version=__version__)
    parser.add_argument(
        "source",
        help="directory | …/id_ed25519 | …/id_rsa",
        type=Path,
    )
    source = parser.parse_args(arguments).source

    if source.is_dir():
        for file in NAMES:
            key = source / file
            if key.is_file():
                source = key
                break

    if not (source.is_file() and source.exists()):
        print(f"No private key found — {source}\n\n\n")
        parser.print_help()
        return 1

    with TemporaryDirectory() as directory:
        destination = Path(directory) / source.name
        copy(source, destination)
        destination.chmod(0o400)
        try:
            run(("ssh-add", destination), check=True)
        except KeyboardInterrupt:
            return 1

    return 0


if __name__ == "__main__":
    raise SystemExit(main())

# tests.py
# Copyright 2023 Keith Maxwell
# SPDX-License-Identifier: MPL-2.0
import contextlib
import sys
import unittest
from io import StringIO
from pathlib import Path
from tempfile import TemporaryDirectory
from unittest.mock import ANY
from unittest.mock import patch

from ssh_add_insecure import __version__
from ssh_add_insecure import main


@contextlib.contextmanager
def captured_output():
    new_out, new_err = StringIO(), StringIO()
    old_out, old_err = sys.stdout, sys.stderr
    try:
        sys.stdout, sys.stderr = new_out, new_err
        yield sys.stdout, sys.stderr
    finally:
        new_out.seek(0)
        new_err.seek(0)
        sys.stdout, sys.stderr = old_out, old_err


class MainTests(unittest.TestCase):
    def test_no_command_line_arguments(self):
        exited = False
        with captured_output() as (_, err):
            try:
                main([])
            except SystemExit:
                exited = True

        self.assertTrue(exited)
        self.assertIn("usage", err.read().lower())

    def test_help(self):
        exited = False
        with captured_output() as (out, _):
            try:
                main(["--help"])
            except SystemExit:
                exited = True

        self.assertTrue(exited)
        self.assertIn("usage", out.read().lower())

    def test_version(self):
        exited = False
        with captured_output() as (out, _):
            try:
                main(["--version"])
            except SystemExit:
                exited = True

        self.assertTrue(exited)
        self.assertIn(__version__, out.read())

    def test_file(self):
        with TemporaryDirectory() as directory:
            source = Path(directory) / "id_rsa"
            source.touch(0o777)
            with patch("ssh_add_insecure.run") as mock:
                result = main([str(source)])
            mock.assert_called_once_with(("ssh-add", ANY), check=True)
            self.assertEqual(result, 0)

    def test_directory(self):
        with TemporaryDirectory() as directory:
            source = Path(directory)
            (source / "id_rsa").touch(0o777)
            with patch("ssh_add_insecure.run") as mock:
                result = main([str(source)])
            mock.assert_called_once_with(("ssh-add", ANY), check=True)
            self.assertEqual(result, 0)

    def test_interrupted(self):
        with TemporaryDirectory() as directory:
            source = Path(directory) / "id_rsa"
            source.touch(0o777)
            with patch("ssh_add_insecure.run", side_effect=KeyboardInterrupt) as mock:
                result = main([str(source)])
            mock.assert_called_once_with(("ssh-add", ANY), check=True)
            self.assertEqual(result, 1)

    def test_missing_file(self):
        with TemporaryDirectory() as directory, captured_output() as (out, _):
            source = Path(directory) / "id_rsa"
            try:
                return_code = main([str(source)])
            except SystemExit:
                return_code = None
        message = out.read().lower()

        self.assertEqual(1, return_code)
        self.assertIn("usage", message)
        self.assertIn("no private key found", message)


if __name__ == "__main__":
    unittest.main()
